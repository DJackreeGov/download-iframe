<?php
    $dsn = "mysql:dbname=cli_dailydata; host=localhost";
    $user = 'cli_user'; // your database username
    $password = 'mer500bs'; // your database password
    
    // note: do not use root, but create a separate user for the database.
    
    try {
        $dbh = new PDO($dsn, $user, $password);
        // print_r($dbh);
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
    
    //this depends on the data in your database.
    $sql = "SELECT * FROM Piarco";
    
    $sth = $dbh->prepare($sql);
    $sth -> execute();
    $results = $sth -> fetchAll(PDO::FETCH_OBJ);
    // print_r($results);

    function array_to_csv_download($array, $filename = "export.csv", $delimiter=",") {
        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w'); 

        //Outputting the Column Names 
        $columnNames = array('Year', 'Month', 'Day', 'Precipitation', 'Min Temperature', 'Max Temperature');
        fputcsv($f, $columnNames, $delimiter);
        
        // loop over the input array
        foreach ($array as $line) { 
            if( is_object($line) ){
                $line = (array) $line;
            }

                // generate csv lines from the inner arrays
                $lineValues = array( $line["Year"], $line["Month"], $line["Day"], $line["PRECIP"], $line["MAXTEMP"], $line["MINTEMP"]);
                fputcsv($f, $lineValues, $delimiter); 

                
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: text/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
        // fclose($f);
        die();
    }
    if(array_key_exists('test',$_POST)){
        array_to_csv_download($results, "Piarco_Climate_Data.csv");
     }

    echo '<form method="post">
    <input type="submit" name="test" id="test" value="Download" style="background-color: #008CBA;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;" /><br/>
</form>';



    
?>
