<?php
    $dsn = "mysql:dbname=cli_dailydata; host=localhost";
    $user = 'cli_user'; // your database username
    $password = 'mer500bs'; // your database password
    
    // note: do not use root, but create a separate user for the database.
    
    try {
        $dbh = new PDO($dsn, $user, $password);
        // print_r($dbh);
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
    
    //this depends on the data in your database.
    $sql = "SELECT * FROM location_data";
    
    
    // print_r($dbh->query($sql)->fetchAll());
    $sth = $dbh->prepare($sql);
    $sth -> execute();
    $results = $sth -> fetchAll(PDO::FETCH_OBJ);
    // print_r($results);
    function array_to_csv_download($array, $filename = "export.csv", $delimiter=",") {
        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w'); 

        //Outputting the Column Names 
        $columnNames = array('Year', 'Month', 'Day', 'Precipitation', 'Min Temperature', 'Max Temperature');
        fputcsv($f, $columnNames, $delimiter);
        
        // loop over the input array
        foreach ($array as $line) { 
            if( is_object($line) )
                $line = (array) $line;
            // generate csv lines from the inner arrays
            $lineValues = array($line["locID"], $line["day"], $line["month"], $line["year"], $line["maxTemp"], $line["minTemp"], $line["precipitation"]);

            fputcsv($f, $lineValues, $delimiter); 
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: text/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
    }
    // function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
    //     header('Content-Type: application/csv');
    //     header('Content-Disposition: attachment; filename="'.$filename.'";');
    
    //     // open the "output" stream
    //     // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
    //     $f = fopen('php://output', 'w');
    
    //     foreach ($array as $line) {
    //         if( is_object($line) )
    //             $line = (array) $line;
    //         fputcsv($f, $line, $delimiter);
    //     }
    // } 
    // array_to_csv_download($results, "cli_data.csv");
    // if (isset($_POST['action'])) { 
    //     array_to_csv_download($results, "cli_data.csv");
    // }
    if(array_key_exists('test',$_POST)){
        array_to_csv_download($results, "cli_data.csv");
     }

    echo '<form method="post">
    <input type="submit" name="test" id="test" value="Download" style="background-color: #008CBA;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;" /><br/>
</form>';
?>
<!-- <script type="text/javascript">
    $(document).ready(function(){ 
        $('.button').click(function(){ 
            var clickBtnValue = $(this).val(); 
            var ajaxurl = 'ajax.php', 
            data =  {'action': clickBtnValue}; 
            $.post(ajaxurl, data, function (response) { 
                // Response div goes here. 
                alert("action performed successfully"); 
            }); 
        }); 
     
    }); 
</script> -->
<!-- <form method="post">
    <input type="submit" name="test" id="test" value="Download" style="background-color: #008CBA;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;" /><br/>
</form> -->

<!-- <style>
    .button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.button2 {background-color: #008CBA;} /* Blue */
</style> -->